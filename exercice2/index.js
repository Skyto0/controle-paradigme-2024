//maskify('4556364607935616') -> '############5616'
//maskify('1') -> '1'
//maskify('11111') -> '#1111'

function maskify(cc) {
    if (cc.length <= 4) {
        return cc;
    }
    let lastFour = cc.slice(-4);
    let masked = '#'.repeat(cc.length - 4);
    return masked + lastFour;
}


//Valeurs changables

console.log(maskify('4556364607935616'));
console.log(maskify('1'));
console.log(maskify('11111'));
