//Filter_list ([1,2, 'a', 'b']) -> [1,2]
//Filter_list ([1, 'a', 'b', 0,15]) -> [1,0,15]
//Filter_list ([1,2, 'aasf', '3', '124', 123]) -> [1,2,123]


function filter_list(l) {
    return l.filter(item => typeof item === 'number');
}


console.log(filter_list([1, 2, 'a', 'b']));
console.log(filter_list([1, 'a', 'b', 0, 15]));
console.log(filter_list([1, 2, 'aasf', '3', '124', 123]));
