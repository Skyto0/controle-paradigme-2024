//DNAStrand ("ATTGC") -> "TAACG"
//DNAStrand ("GTAT") -> "CATA"



function DNAStrand(dna) {
    const complements = {
        'A': 'T',
        'T': 'A',
        'C': 'G',
        'G': 'C'
    };

    return dna.replace(/A|T|C|G/g, match => complements[match]);
}


console.log(DNAStrand("ATTGC"));
console.log(DNAStrand("GTAT"));
