package principal;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		List<Personne> personnes = new ArrayList<>();

		personnes.add(new Membre("Flo", "Limane", 1, new Date(), "Actif"));
		personnes.add(new Membre("Ludovic", "Kamesh", 2, new Date(), "Inactif"));

		personnes.add(new Employe("PORLIER-PAGNON", "Florent", 3, "Directeur", 4500.0));
		personnes.add(new Employe("TURNA", "Musa", 4, "Secrétaire", 2200.0));
		
		for (Personne p : personnes) {
			p.afficherDetails();
		}
	}
}
