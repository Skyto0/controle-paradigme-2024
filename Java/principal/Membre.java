package principal;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

class Membre extends Personne {
    private Date dateAdhesion;
    private String statut;
    private List<Livre> emprunts;

    public Membre(String nom, String prenom, int id, Date dateAdhesion, String statut) {
        super(nom, prenom, id);
        this.dateAdhesion = dateAdhesion;
        this.statut = statut;
        this.emprunts = new ArrayList<>();
    }


    public void emprunter(Livre livre) {
        if (livre != null && livre.isDisponible()) {
            emprunts.add(livre);
            livre.emprunter();
            System.out.println("Le livre '" + livre.getTitre() + "' a été emprunté par " + getPrenom() + " " + getNom() + ".");
        } else {
            System.out.println("Le livre n'est pas disponible pour l'emprunt.");
        }
    }
    
    public void retourner(Livre livre) {
        if (livre != null && emprunts.contains(livre)) {
            emprunts.remove(livre);
            livre.retourner();
            System.out.println("Le livre '" + livre.getTitre() + "' a été emprunté par " + getPrenom() + " " + getNom() + ".");
        } else {
            System.out.println("Le livre est disponible pour l'emprunt.");
        }
    }
    public Date getDateAdhesion() {
        return dateAdhesion;
    }

    public void setDateAdhesion(Date dateAdhesion) {
        this.dateAdhesion = dateAdhesion;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public List<Livre> getEmprunts() {
        return emprunts;
    }

    @Override
    public void afficherDetails() {
        System.out.println("Nom: " + getNom() + ", Prénom: " + getPrenom() + ", ID: " + getId()
                + ", Date d'adhésion: " + getDateAdhesion() + ", Statut: " + getStatut()
                + ", Nombre d'emprunts: " + emprunts.size());
    }
}
