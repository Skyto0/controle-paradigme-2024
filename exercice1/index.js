//number([[10,0],[3,5],[5,8]]) -> 5
//number([[3,0],[9,1],[4,10],[12,2],[6,1],[7,10]]) -> 17
//number([[3,0],[9,1],[4,8],[12,2],[6,1],[7,8]]) -> 21

function number(arretsBus) {
    let totalPeople = 0;
    for (let i = 0; i < arretsBus.length; i++) {
        totalPeople += arretsBus[i][0] - arretsBus[i][1];
    }
    return totalPeople;
}


console.log(number([[10,0],[3,5],[5,8]]));
console.log(number([[3,0],[9,1],[4,10],[12,2],[6,1],[7,10]]));
console.log(number([[3,0],[9,1],[4,8],[12,2],[6,1],[7,8]]));
